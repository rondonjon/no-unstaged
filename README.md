# no-unstaged

A small script that terminates the process with status code 1 if the working directory (pwd) contains unstaged files.

Intended for use in pre-commit hooks to ensure that they won't run on unstaged changes which could then be missing in the actual commit.

Based on [simple-git](https://www.npmjs.com/package/simple-git).

## Usage

Install:

```
npm i -D rondonjon-no-unstaged
```

In your `package.json`:

```
...
  scripts: {
    "precommit": "no-unstaged && <...lint, tsc or whatever you want to check>",
    ...
  }
...
```

## License

MIT License

Copyright © 2022 "Ron Don Jon" Daniel Kastenholz.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## History

| Version | Changes                     |
| ------- | --------------------------- |
| 1.0.0   | Initial version             |
| 1.1.0   | Add usage info to README.md |
