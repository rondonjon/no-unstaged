#!/usr/bin/env node
const simpleGit = require("simple-git")
const git = simpleGit()

git
  .status()
  .then(({ files }) => {
    const unstaged = files.filter(({ working_dir }) => working_dir !== " ")
    if (unstaged.length) {
      const paths = unstaged.map(({ path }) => path)
      throw new Error("Unstaged changes detected!\n" + paths.join("\n"))
    }
  })
  .catch((err) => {
    console.error(err)
    process.exit(1)
  })
